/* global require */
/* global __dirname */
var gulp    = require('gulp');
var uglify  = require('gulp-uglify');
var ngtpl   = require('gulp-angular-templatecache');
var concat  = require('gulp-concat');
var order   = require('gulp-order');

gulp.task('tpl', function () {
    return gulp.src(__dirname + '/assets/src/templates/**/*.html')
        .pipe(ngtpl({module: 'angulartable.modules.templates'}))
        .pipe(gulp.dest(__dirname + '/assets/src/js'));
});
gulp.task('css', function () {
    return gulp.src(__dirname + '/assets/src/css/**/*.css')
        .pipe(gulp.dest(__dirname + '/assets/dist/css'));
});
gulp.task('js', ['tpl'], function() {
    return gulp.src(__dirname + '/assets/src/js/**/*.js')
        .pipe(order([
            'app.js',
            'services/**/*.js',
            'modules/**/*.js',
            'directives/**/*.js',
            'controllers/**/*.js',
            '**/*.js',
            'templates.js'
        ]))
        .pipe(concat('app.js'))
        //.pipe(uglify())
        .pipe(gulp.dest(__dirname + '/assets/dist/js'));
});

gulp.task('watch', function() {
    gulp.watch([
        __dirname + '/assets/src/templates/**/*.html'
    ], [
        'templates'
    ]);
    gulp.watch([
        __dirname + '/assets/src/js/**/*.js'
    ], [
        'ppp'
    ]);
});

gulp.task('build', ['js', 'css']);
gulp.task('default', ['build']);