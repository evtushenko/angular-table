(function() {
    angular.module('angulartable', [
        'angulartable.services',
        'angulartable.controllers'
    ]);
})();