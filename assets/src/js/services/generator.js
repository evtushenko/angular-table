(function() {
    angular.module('angulartable.services', [])
        .factory('generatorService', [function() {
            return function(rows) {
                return _.map(_.range(0, rows), function(i) {
                    return {
                        id:     i + 1,
                        name:   faker.name.firstName(),
                        date:   (function(years) {
                            var date = new Date();
                            var range = {
                                min: 1000,
                                max: (years || 1) * 365 * 24 * 3600 * 1000
                            };

                            var past = date.getTime();
                            past -= faker.random.number(range);
                            date.setTime(past);

                            return date;
                        })(5),
                        count:  parseInt(Math.random() * 1000)
                    };
                });
            };
        }]);
})();