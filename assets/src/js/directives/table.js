(function() {
    angular.module('angulartable.directives').directive('atTable', [
        '$filter', '$templateCache',
        function($filter, $templateCache) {
            return {
                restrict: 'A',
                template: $templateCache.get('directives/table.html'),
                replace: true,
                scope: {
                    items: '=',
                    pageSize: '@'
                },
                link: function($scope) {
                    $scope.rows = [];
                    $scope.pageSize = parseInt($scope.pageSize) || 10;
                    $scope.ctrl.update();
                },
                controller: function($scope) {
                    var ctrl = $scope.ctrl = this;
                    _.extend(ctrl, {
                        sortFn: null,
                        filtersFn: {},

                        update: function() {
                            $scope.rows = (function(items) {
                                items = _.filter(items, function(item) {
                                    return _.reduce(ctrl.filtersFn, function(memo, filterFn) {
                                        return memo && filterFn(item);
                                    }, true);
                                });

                                items = items.sort(ctrl.sortFn);

                                return items.slice(0, $scope.pageSize);
                            })($scope.items);
                        },
                        applySortFn: function(sortFn) {
                            if (!sortFn) {
                                ctrl.sortFn = null;
                            } else {
                                ctrl.sortFn = sortFn;
                            }

                            ctrl.update();
                        },
                        applyFilterFn: function(col, filterFn) {
                            if (!filterFn) {
                                delete ctrl.filtersFn[col];
                            } else {
                                ctrl.filtersFn[col] = filterFn;
                            }

                            ctrl.update();
                        }
                    });
                }
            };
        }
    ]);
})();