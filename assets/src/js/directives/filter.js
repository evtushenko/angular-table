(function() {
    angular.module('angulartable.directives').directive('atFilter', [
        '$timeout', '$templateCache',
        function($timeout, $templateCache) {
            return {
                restrict: 'A',
                template: $templateCache.get('directives/filter.html'),
                replace: true,
                scope: {
                    items: '=',
                    valueGetter: '&',
                    applyFilterFn: '&'
                },
                link: function ($scope, $element) {
                    var $select = $element.find('select').multiselect({
                        maxHeight: 300,
                        dropRight: true,
                        buttonClass: 'btn btn-link',
                        buttonText: function() {
                            return '<i class="glyphicon glyphicon-filter"></i>'
                        }
                    });
                    $scope.$watch('items', function() {
                        $scope.model.values = $scope.getValues();
                        $timeout(function() {
                            $select.multiselect('rebuild');
                        });
                    });
                    $scope.$watch('model.checked', function() {
                        $scope.applyFilterFn({filter: $scope.getFilterFn()});
                    });

                    _.extend($scope, {
                        model: {
                            values: [],
                            checked: []
                        },

                        getFilterFn: function () {
                            return function (item) {
                                if (!$scope.model.checked.length) {
                                    return true;
                                }

                                return _.indexOf($scope.model.checked, $scope.valueGetter({item: item})) >= 0;
                            };
                        },
                        getValues: function() {
                            return _.unique(_.map($scope.items, function(item) {
                                return $scope.valueGetter({item: item});
                            }));
                        }
                    })
                }
            };
        }
    ]);
})();