(function() {
    angular.module('angulartable.directives').directive('atSorter', [
        '$templateCache',
        function($templateCache) {
            return {
                restrict: 'A',
                template: $templateCache.get('directives/sorter.html'),
                replace: true,
                scope: {
                    applySortFn: '&',
                    valueGetter: '&'
                },
                link: function($scope) {
                    var DIRECTION = {
                        NONE:   0,
                        ASC:    1,
                        DESC:   -1
                    };

                    _.extend($scope, {
                        direction: DIRECTION.NONE,
                        toggle: function() {
                            if ($scope.direction === DIRECTION.NONE) {
                                $scope.direction = DIRECTION.ASC;
                            } else if ($scope.direction === DIRECTION.ASC) {
                                $scope.direction = DIRECTION.DESC;
                            } else {
                                $scope.direction = DIRECTION.NONE;
                            }

                            $scope.applySortFn({comparator: $scope.getSortFn()});
                        },
                        getIndicatorClass: function() {
                            return {
                                'glyphicon-sort':       $scope.direction === DIRECTION.NONE,
                                'glyphicon-menu-up':    $scope.direction === DIRECTION.ASC,
                                'glyphicon-menu-down':  $scope.direction === DIRECTION.DESC
                            };
                        },
                        getSortFn: function() {
                            if ($scope.direction === DIRECTION.NONE) {
                                return null;
                            }

                            return function(a, b) {
                                a = $scope.valueGetter({item: a});
                                b = $scope.valueGetter({item: b});

                                if (a === b) {
                                    return 0;
                                } else if (a > b) {
                                    return 1 * $scope.direction;
                                } else {
                                    return -1 * $scope.direction;
                                }
                            };
                        }
                    });
                }
            };
        }
    ]);
})();