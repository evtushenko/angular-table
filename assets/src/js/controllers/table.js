(function() {
    angular.module('angulartable.controllers')
        .controller('tableController', [
            'generatorService',
            function(generatorService) {
                var ctrl = this;
                _.extend(ctrl, {
                    rows: generatorService(100)
                });
            }
        ]);
})();